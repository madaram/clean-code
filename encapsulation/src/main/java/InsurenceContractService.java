
public class InsurenceContractService {

	public boolean isInsuredAgainst(Insuree insuree, AccidentType accidentType) {
		for (Insurance contrat : insuree.getInsurances()) {
			if (contrat.getAccidentType() == accidentType) {
				return true;
			}
		}
		return false;
	}

	public void subcribeToContract(Insuree insuree, AccidentType accidentType) {
		Insurance contrat = new Insurance();
		contrat.setAccidentType(accidentType);
		insuree.getInsurances().add(contrat);
	}
}
