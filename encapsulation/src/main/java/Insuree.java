import java.util.HashSet;
import java.util.Set;

public class Insuree {

	private Set<Insurance> insurances = new HashSet<Insurance>();

	public Set<Insurance> getInsurances() {
		return insurances;
	}

	public void setInsurances(Set<Insurance> insurances) {
		this.insurances = insurances;
	}

}
