
public class Insurance {

	private AccidentType accidentType;

	public AccidentType getAccidentType() {
		return this.accidentType;
	}

	public void setAccidentType(AccidentType accidentType) {
		this.accidentType = accidentType;
	}
}
