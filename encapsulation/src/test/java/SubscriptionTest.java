
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SubscriptionTest {

	private final InsurenceContractService insurenceContractService = new InsurenceContractService();

	@Test
	public void an_insuree_without_contract_is_not_insured_against_flood() {
		// given
		Insuree monsieurDupont = new Insuree();
		// when then
		assertFalse(insurenceContractService.isInsuredAgainst(monsieurDupont, AccidentType.FLOOD));
	}

	@Test
	public void an_insuree_with_contract_against_flood_is_insured_against_flood() {
		// given
		Insuree monsieurDupont = new Insuree();
		insurenceContractService.subcribeToContract(monsieurDupont, AccidentType.FLOOD);
		// when then
		assertTrue(insurenceContractService.isInsuredAgainst(monsieurDupont, AccidentType.FLOOD));
	}

	@Test
	public void an_insuree_with_contract_againstcar_accident_is_not_insured_against_flood() {
		// given
		Insuree monsieurDupont = new Insuree();
		insurenceContractService.subcribeToContract(monsieurDupont, AccidentType.CAR_ACCIDENT);
		// when then
		assertFalse(insurenceContractService.isInsuredAgainst(monsieurDupont, AccidentType.FLOOD));
	}

}
