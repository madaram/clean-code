package fr.capgemini.clean.primitive;
import java.util.Set;

public interface Rooms {

	Set<Room> findRoomsByCapacite(int personNumber);

}
