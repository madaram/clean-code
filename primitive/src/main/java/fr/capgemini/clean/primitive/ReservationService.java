package fr.capgemini.clean.primitive;
import java.time.LocalDate;
import java.util.Optional;

public class ReservationService {

	private final Rooms rooms;      	
	private final Reservations reservations;

	public ReservationService(Rooms rooms, Reservations reservations) {
		this.rooms = rooms;
		this.reservations = reservations;
	}

	public Optional<Room> findRoomReservable(int personNumber, LocalDate day, int startingHour, int endingHour) {
		return rooms.findRoomsByCapacite(personNumber)
				.stream()
				.filter(room -> isReservationPossible(room, day, startingHour, endingHour))
				.findAny();
	}

	private boolean isReservationPossible(Room room, LocalDate day, int startingHour, int endingHour) {		
		return reservations.findReservations(room, day)
				.stream()
				.noneMatch(reservation -> chevauche(reservation, day, startingHour, endingHour));
	}

	private boolean chevauche(Reservation reservation, LocalDate day, int startingHour, int endingHour) {
		return reservation.getEndingHour() > startingHour
				&& reservation.getStartingHour() < endingHour;
	}

}
