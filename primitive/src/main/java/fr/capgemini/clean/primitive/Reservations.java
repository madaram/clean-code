package fr.capgemini.clean.primitive;
import java.time.LocalDate;
import java.util.Set;

public interface Reservations {

	Set<Reservation> findReservations(Room room, LocalDate date);

}
