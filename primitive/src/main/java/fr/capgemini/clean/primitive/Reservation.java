package fr.capgemini.clean.primitive;
import java.time.LocalDate;

public class Reservation {
    
    private final LocalDate day;
    private final int startingHour, endingHour;

    public Reservation(LocalDate day, int startingHour, int endingHour) {
        this.day = day;
        this.startingHour = startingHour;
        this.endingHour = endingHour;
    }

	public LocalDate getDay() {
		return day;
	}

	public int getStartingHour() {
		return startingHour;
	}

	public int getEndingHour() {
		return endingHour;
	}

}
