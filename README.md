# README #

Contains code to illustrate clean code presentation I did. 

# Requirements #

 * java 8 (tested with 1.8.0_60)
 * Apache Maven 3.*.* (tested with Apache Maven 3.0.5)
 
# Installation #

mvn clean install

