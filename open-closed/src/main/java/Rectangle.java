public class Rectangle {

	final public double width;
	final public double height;

	public Rectangle(double width, double height) {
		this.width = width;
		this.height = height;
	}
}
