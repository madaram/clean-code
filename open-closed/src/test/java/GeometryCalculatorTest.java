import org.junit.Assert;
import org.junit.Test;

public class GeometryCalculatorTest {

	private static final double DELTA = 0.0001;

	private GeometryCalculator areaCalculator = new GeometryCalculator();

	@Test
	public void testArea() {
		// GIVEN
		double width = 5;
		double height = 6;
		Rectangle rectangle = new Rectangle(width, height);
		double radius = 10;
		Circle circle = new Circle(radius);
		Object[] shapes = new Object[]{rectangle, circle};
		// WHEN
		double actual = areaCalculator.area(shapes);
		// THEN
		Assert.assertEquals(Math.PI * radius * radius + width * height, actual, DELTA);

	}
}
