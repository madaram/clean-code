import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class GeneratePrimesTest {


	@Test
	public void testGeneratePrimes() {
		int[] expected = {2, 3, 5, 7, 11, 13};
		int[] actuals = GeneratePrimes.generatePrimes(13);
		System.out.print(Arrays.toString(actuals));
		Assert.assertArrayEquals(expected, actuals);

	}
}
