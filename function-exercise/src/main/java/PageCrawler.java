public interface PageCrawler {
	WikiPagePath getFullPath(WikiPage page);
}
